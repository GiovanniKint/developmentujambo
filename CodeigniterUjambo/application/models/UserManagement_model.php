<?php

class UserManagement_model extends CI_Model
{
    public function getUserData()
    {
        $query = $this->db->query("SELECT Username FROM User");

        return $query->result();
    }

    public function getRoleData()
    {
        $query = $this->db->query('SELECT Roles_id, Name FROM Roles');


        return $query->result();
    }


}