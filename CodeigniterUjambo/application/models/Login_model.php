<?php

class Login_model extends CI_Model
{

    public function check_login($username, $password)
    {

        $username = $this->security->xss_clean($username);
        $password = $this->security->xss_clean($password);

        $query = $this->db->query("SELECT * FROM User WHERE Username = '$username'");

        $isValidated = false;

        foreach ($query->result_array() as $row) {
            if ($username == $row['Username']) {
                if ($password = password_verify($password, $row['Password'])) {
                    $isValidated = true;
                }
            }
        }

        return $isValidated;
    }

}