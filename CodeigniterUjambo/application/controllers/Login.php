<?php

class Login extends CI_Controller
{

    public function index() //loads standard login form
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('head');
        $this->load->view('/Authentication/login');
    }

    public function userLogin() //takes input from login form and chekcs if user may login
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $this->load->model('Login_model');

        $isValidated = $this->Login_model->check_login($username, $password);

        if ($isValidated != false) {
            $this->session->set_userdata('user', $username); //sets session username

            $this->load->helper('url');
            redirect('/Dashboard'); //redirects to dashboard
        } else {
            $this->load->helper('url');
            redirect('/login/userLogout'); //redirects to the logout function
        }


    }

    public function userLogout() //destroys session and redirects to login page
    {
        session_start();
        session_destroy();
        $this->load->helper('url');
        redirect('/login');
    }

}