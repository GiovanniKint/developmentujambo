<?php

class UserManagement extends CI_Controller
{

    public function index()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('head');
        $this->load->view('navbar');
    }

    public function permissionOverview()
    {
        $this->load->view('head');
        $this->load->view('navbar');

        $this->load->view('Usermanagement/permissionOverviewTable');
    }

    public function permissionCreate()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('head');
        $this->load->view('navbar');

        $this->load->view('Usermanagement/permissionCreateForm');
    }

    public function permissionCreateSend()
    {
        $name = $this->input->post("name");
        $description = $this->input->post("description");

        $name = $this->security->xss_clean($name);
        $description = $this->security->xss_clean($description);


        $data = array(
            'Name' => $name,
            'Description' => $description
        );

        $this->db->insert('Permissions', $data);

        $this->load->helper('url');
        redirect('/UserManagement/permissionOverview');

    }

    public function roleOverview()
    {
        $this->load->view('head');
        $this->load->view('navbar');

        $this->load->view('Usermanagement/roleOverviewTable');
    }

    public function roleCreate()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('head');
        $this->load->view('navbar');

    }

    public function userOverview()
    {
        $this->load->view('head');
        $this->load->view('navbar');

        $this->load->view('Usermanagement/userOverviewTable');
    }

    public function userCreate()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('head');
        $this->load->view('navbar');

        $this->load->model('UserManagement_model');

        $data['roleData'] = $this->UserManagement_model->getRoleData();

        $this->load->view('Usermanagement/userCreateForm', $data);
    }

    public function userCreateSend()
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $username = $this->security->xss_clean($username);
        $password = $this->security->xss_clean($password);

        $password = password_hash($password, PASSWORD_DEFAULT);

        $role = $this->input->post("role");

        $this->load->model('UserManagement_model');
        $usernames = $this->UserManagement_model->getUserData();

        $isAviable = true;

        foreach ($usernames as $username1) {
            if ($username == $username1->Username) {
                $isAviable = false;
            }
        }

        if ($isAviable) {
            $data = array(
                'Username' => $username,
                'Password' => $password,
                'Roles_id' => $role
            );

            $this->db->insert('User', $data);

            $this->load->helper('url');
            redirect('/UserManagement/userOverview');
        } else {
            $this->load->helper('url');
            redirect('/UserManagement/userCreate');
        }

        $this->load->view('head');
        $this->load->view('navbar');
    }

}