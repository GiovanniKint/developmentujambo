<head>
    <meta charset="utf-8">
    <link href="<?= base_url('assets/css/materialize.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="<?= base_url('assets/js/materialize.min.js') ?>"></script>
    <script language="JavaScript" type="text/javascript" src="<?= base_url('assets/js/documentready.js') ?>"></script>
</head>