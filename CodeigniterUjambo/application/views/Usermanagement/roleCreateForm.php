<div class=" container">
    <h4 class="center-align">User create</h4>
    <div class="divider"></div>

    <div class="valign-wrapper">
        <div class="valign supercenter">
            <div class="center-align">
                <?php echo form_open('UserManagement/userCreateSend'); ?>

                <?php echo form_input('email', '', ' type="email" class="email center-align" placeholder="Email" required'); ?>

                <?php echo form_input('username', '', 'class="center-align" placeholder="Username" required'); ?>

                <?php echo form_password('password', '', 'class="center-align" placeholder="Password" required'); ?>

                <?php echo form_input('name', '', 'class="center-align" placeholder="Name" required'); ?>

                <?php echo form_input('surname', '', 'class="center-align" placeholder="Surname" required'); ?>

                <?php $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]'); ?>

                <select name="role" required>


                <?php

                foreach ($roleData as $row) {
                    echo '<option value="' . $row->Roles_id . '" class=\'text-pink accent-2\'>' . $row->Name . '</option>';
                };

                ?>

                <?php echo form_input('company_id', '', 'class="center-align" placeholder="COMPANY ID MOET NOG EEN SELECTIE VAK WORDEN" required'); ?>

                </select>

                <button class="btn waves-effect waves-light fullwidth pink lighten-1" type="submit"
                        name="action">
                    Submit
                </button>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
