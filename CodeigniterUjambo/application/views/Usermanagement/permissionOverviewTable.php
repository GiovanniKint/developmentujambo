<div class="container">

    <h4 class="center-align">User overview</h4>
    <div class="divider"></div>

    <table class="centered highlight">
        <thead>
        <tr>
            <th data-field="id">Name</th>
            <th data-field="name">ID</th>
            <th data-field="name">Role</th>
            <th data-field="price">Edit</th>
        </tr>
        </thead>

        <tbody>


        <?php


        $query = $this->db->query("SELECT * FROM Permissions");

        foreach ($query->result() as $row) {
            ?>
            <tr>
                <td> <?php echo $row->Name; ?> </td>
                <td> <?php echo $row->Permissions_id; ?> </td>
                <td> <?php echo $row->Description; ?> </td>
            </tr>
            <?php
        }

        ?>

        </tbody>
    </table>
</div>