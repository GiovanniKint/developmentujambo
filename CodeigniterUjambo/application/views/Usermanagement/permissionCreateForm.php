<div class=" container">
    <h4 class="center-align">User create</h4>
    <div class="divider"></div>

    <div class="valign-wrapper">
        <div class="valign supercenter">
            <div class="center-align">
                <?php echo form_open('UserManagement/permissionCreateSend'); ?>

                <?php echo form_input('name', '', 'class="center-align" placeholder="Name" required'); ?>
                <?php echo form_input('description', '', 'class="center-align" placeholder="Description" required'); ?>

                <button class="btn waves-effect waves-light fullwidth pink lighten-1" type="submit"
                        name="action">
                    Submit
                </button>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
