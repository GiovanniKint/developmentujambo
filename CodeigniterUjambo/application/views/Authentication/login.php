<div>
    <div class="row nomargin">
        <div class="col s3 fullheight valign-wrapper">
            <div class="valign">
                <div class="center-align">
                    <h2 class="center-align">Test Ujambo</h2>

                    <?php echo form_open('Login/userLogin'); ?>

                    <?php echo form_input('username', '', 'class="center-align" placeholder="Username" required'); ?>
                    <?php echo form_password('password', '', 'class="center-align" placeholder="Password" required'); ?>

                    <button class="btn waves-effect waves-light fullwidth pink lighten-1" type="submit"
                            name="action">
                        Submit
                    </button>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
        <div class="col s1 fullheight_diagonal"></div>
        <div class="col s8 fullheight pink lighten-1"></div>
    </div>
</div>