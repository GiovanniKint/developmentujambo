<header>
    <nav>
        <div class="nav-wrapper pink lighten-1">
            <ul id="nav-mobile" class="left hide-on-med-and-down">

                <?php

                $dashboardlink = base_url('Dashboard');

                if (true) {
                    echo "<li><a href=\"$dashboardlink\"><i class=\"material-icons left\">dashboard</i>Dashboard</a></li>";
                }

                if (true) {
                    echo "<li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown1\"><i class=\"material-icons left\">stay_current_portrait</i>Asset<i
                                class=\"material-icons right\">arrow_drop_down</i></a></li>";
                }

                if (true) {
                    echo "<li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown2\"><i class=\"material-icons left\">business</i>Company<i
                                class=\"material-icons right\">arrow_drop_down</i></a></li>";
                }

                if (true) {
                    echo "<li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown3\"><i class=\"material-icons left\">perm_media</i>Modules<i
                                class=\"material-icons right\">arrow_drop_down</i></a></li>";
                }

                if (true) {
                    echo "<li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown4\"><i class=\"material-icons left\">supervisor_account</i>User management<i
                                class=\"material-icons right\">arrow_drop_down</i></a></li>";
                }
                ?>
            </ul>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="<?php echo base_url('Login/userLogout'); ?>"><i
                                class="material-icons left">settings_power</i>Logout</a></li>
            </ul>
        </div>

        <ul id="dropdown1" class="dropdown-content">
            <li><a href="#" class="pink-text text-lighten-2">Asset Overview</a></li>
            <li class="divider"></li>
            <li><a href="#" class="pink-text text-lighten-2">Asset Create</a></li>
        </ul>

        <ul id="dropdown2" class="dropdown-content">
            <li><a href="#" class="pink-text text-lighten-2">Company Overview</a></li>
            <li class="divider"></li>
            <li><a href="#" class="pink-text text-lighten-2">Company Create</a></li>
        </ul>

        <ul id="dropdown3" class="dropdown-content">
            <li><a href="#" class="pink-text text-lighten-2">Module Overview</a></li>
            <li class="divider"></li>
            <li><a href="#" class="pink-text text-lighten-2">Module Create</a></li>
        </ul>

        <ul id="dropdown4" class="dropdown-content">
            <li><a href="<?php echo base_url('UserManagement/userOverview'); ?> " class="pink-text text-lighten-2">User
                    Overview</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('UserManagement/userCreate'); ?>" class="pink-text text-lighten-2">User
                    Create</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('UserManagement/roleOverview'); ?>" class="pink-text text-lighten-2">Role
                    Overview</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('UserManagement/roleCreate'); ?>" class="pink-text text-lighten-2">Role
                    Create</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('UserManagement/permissionOverview'); ?>" class="pink-text text-lighten-2">Permission
                    Overview</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('UserManagement/permissionCreate'); ?>" class="pink-text text-lighten-2">Permission
                    Create</a></li>
        </ul>

    </nav>
</header>