<footer class="page-footer pink lighten-1">
    <div class="footer-copyright">
        <div class="container">
            &copy; 2017/03/06 - <?php echo date("Y"); ?> Giovanni Kint
        </div>
    </div>
</footer>
