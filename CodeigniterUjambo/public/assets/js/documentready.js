$(document).ready(function () {

    $('.modal').modal();

    $(".dropdown-button").dropdown({hover: true});

    $('.collapsible').collapsible();

    $('select').material_select();

    $('#example tr').click(function () {
        var href = $(this).find("a").attr("href");
        if (href) {
            window.location = href;
        }
    });

});